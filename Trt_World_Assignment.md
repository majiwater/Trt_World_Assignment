```python
"""
In this project, I have tried to show how different tools and methods
are useful for exploring, descrbing, visualizing and classfying tweet data.

This is a sample work for Twitter data usage. I have used different tools at 
introductory level for time saving and demonstrating different tools' 
different benefits purposes.
"""

#At first, I've imported Pyhon Libraries. Since there are lots of modules
# I will explain why imported and how i used the library when I use it.

import datetime
import numpy as np
import pandas as pd
from tweepy import OAuthHandler
from tweepy import API
from tweepy import Cursor
import matplotlib.pyplot as plt
import seaborn as sns
from textblob import TextBlob
import re
%matplotlib inline
from wordcloud import WordCloud, STOPWORDS
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from lightgbm import LGBMClassifier
import pylab as pl
from sklearn.neighbors import KNeighborsClassifier

```


```python
#After getting my Twitter account's token and secrets, I used Tweepy library
#The library is simple to use
#By using Cursor class of the module I got more than 3000 tweets from TRT World's account


key="My Key"
secret="My Secret Key"
token="Token Key"
token_secret="Token Secret"
auth = OAuthHandler(key, secret)
auth.set_access_token(token, token_secret)
api = API(auth)
tweet=Cursor(api.user_timeline, screen_name="trtworld").pages()

```


```python
#I wanted to save the time of work is done
time=datetime.datetime.now()
time
""
```




    datetime.datetime(2018, 7, 24, 1, 0, 44, 537972)




```python
"""
This stage may be the most important part of the work since all data are downloaded at this stage
While I was looking for the data I wanted to get the right data for the analysis
For this purpose, I just got the most basic ones tweets' favorites, retweets and tweets themselves
"""

fav=[]
rt=[]
text=[]
for i in tweet:
    for j in i:    
        fav.append(j.favorite_count)
        rt.append(j.retweet_count)
        text.append(j.text)
        

```


```python
for i in np.arange(len(text)):
    text[i]=m =  re.sub(r"http\S+", "", text[i])

```


```python
# In this part of the work, I implemented sentiment and subjectivity extraction to  tweets
# I have chosen TextBlob module among many others like Gensim, for simplicity purposes 
# Like others, TextBlob is pre-trained text mining library and therefore I can easily get
# Sentiment and Subjectivity polarity
# Setiment polarity varies from -1 ( The most negative) and 1 (the most positive)
# And of course, 0 polarity means text is neutral
# Furthermore, another column was addded to data which is sentiment polarity scores
# namely has three unique data positive,negative and neutral

sentiment_numeric=[]
sentiment_class=[]
subjectivity=[]
for i in np.arange(len(text)):
    tx=TextBlob(text[i])
    sentiment_numeric.append(tx.sentiment.polarity)
    subjectivity.append(tx.sentiment.subjectivity)

    if tx.sentiment.polarity==0:
        sentiment_class.append('neutral')
    elif tx.sentiment.polarity<0:
        sentiment_class.append('negative')
    else:
        sentiment_class.append('positive')
        
```


```python
# In order to make some descriptive analysis on the data, I turned them tabular data format, pandas dataframe
data=pd.DataFrame(list(zip(sentiment_class, sentiment_numeric, subjectivity,fav,rt)),
                  columns=['sentiment_class','sentiment_numeric','subjectivity','favorites'
                          ,'retweets'])
# Two subjectivity classes are added to the dataframe, i which the threshold was chosen at 0.5 
subjectivity_class=[]
for i in data["subjectivity"]:
    if i<0.50:
        subjectivity_class.append("objective tweet")
    else:
        subjectivity_class.append("subjective tweet")
data["subjectivity_class"]=subjectivity_class
```


```python
# Now, let's look at max and min of subjectivitiy and emotional polarity in Trt World's tweets

print(" The most positive tweet posted most recent")
print("\n" + text[data.loc[(data.sentiment_numeric==data.sentiment_numeric.max())].index[-1]])
print("\n" + "The most negative tweet posted most recent")
print("\n" + text[data.loc[(data.sentiment_numeric==data.sentiment_numeric.min())].index[-1]])
print("\n" + "The most subjective tweet posted most recent")
print("\n" + text[data.loc[(data.subjectivity==data.subjectivity.max())].index[-1]])
print("\n" + "The most objective tweet posted most recent")
print("\n" + text[data.loc[(data.subjectivity==data.subjectivity.min())].index[-1]])

```

     The most positive tweet posted most recent
    
    France's Kylian Mbappe has been compared to the legendary Pele and is one of the hottest talents at the World Cup.… 
    
    The most negative tweet posted most recent
    
    "Terrifying. (I'm) trying to comprehend the scale," Prince William says as he starts his Israel trip with a visit t… 
    
    The most subjective tweet posted most recent
    
    RT @TRTWorldNow: Erdogan addresses crowds in Ankara thanking the Turkish people for standing by them throughout difficult periods including…
    
    The most objective tweet posted most recent
    
    RT @TRTWorldNow: Here is a look at how world leaders are reacting to President Erdogan's victory in the historic #TurkeyElections 
    


```python
# In the beginning of the analysis, fundamental statistics and graphs are shown
data.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sentiment_numeric</th>
      <th>subjectivity</th>
      <th>favorites</th>
      <th>retweets</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>3207.000000</td>
      <td>3207.000000</td>
      <td>3207.000000</td>
      <td>3207.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>0.024786</td>
      <td>0.247771</td>
      <td>17.745868</td>
      <td>17.061116</td>
    </tr>
    <tr>
      <th>std</th>
      <td>0.219005</td>
      <td>0.281490</td>
      <td>120.141661</td>
      <td>123.577660</td>
    </tr>
    <tr>
      <th>min</th>
      <td>-1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>0.000000</td>
      <td>0.166667</td>
      <td>5.000000</td>
      <td>6.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>0.100000</td>
      <td>0.439313</td>
      <td>11.000000</td>
      <td>11.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>4168.000000</td>
      <td>5366.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
# At the first visualizition, count of tweets' sentiment classes, and then the count plot of subjectivity class are made

sns.countplot(sentiment_class)
plt.title('Sentiment Classes of Tweets')
plt.show()
```


![png](output_9_0.png)



```python
sns.countplot(data.subjectivity_class)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2532bea82e8>




![png](output_10_1.png)



```python
# The numeric columns' boxplots are good statistical visualizations
# However as seen below, even though boxplots are good tools for outlier detection and distribution
# In this data, boxplots are not useful enough since most of data is at zero
# For example, boxplot of retweet numbers are enough i think
sns.boxplot(y=data["retweets"])
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2532c1e12b0>




![png](output_11_1.png)



```python
# In this step, i wanted to make a wordcloud, so i need list of words in all tweets
word_list = [word for line in text for word in line.split()]
word_list=str((word_list))

# Wordcloud library is highly useful for this purpose
stopwords=set(STOPWORDS)
wordcloud = WordCloud(background_color='white',stopwords=stopwords,
                      max_words=200,random_state=1).generate(word_list)
```


```python
#Plotting the word cloud
fig = plt.figure(1, figsize=(12, 12))
plt.axis('off')
plt.imshow(wordcloud)
plt.show
```




    <function matplotlib.pyplot.show>




![png](output_13_1.png)



```python
# Now I'm trying to make an cluster analysis over features other than sentiment polarity features
# So, I've used the most common Machine Learning Library's, scikit-learn, clustering algorithm
# Whilst doing this, I used DBSCAN that looks for denstiy based clusters for non human intervention

scan=DBSCAN()
scan.fit(data.iloc[:,2:-1])
db=pd.DataFrame(scan.components_)
```


```python
# DBSCAN found three different clusters and in these clusters there are almost 1000 data 
# than the original one since if a data does not fit in a density area it is excluded by the algorithm
db.shape
```




    (2302, 3)




```python
# Then, I looked for a visual relation between clusters and emotional classes
# TO see that, I reduced dimensions of clusters to two by principal component analysis
pca = PCA(n_components=2)

principalComponents = pca.fit_transform(db)

```


```python

for i in range(0, principalComponents.shape[0]):
    if sentiment_class[i] == "negative":
        c1 = pl.scatter(principalComponents[i,0], principalComponents[i,1], c='r', marker='+')
    elif sentiment_class[i] == "neutral":
        c2 = pl.scatter(principalComponents[i,0], principalComponents[i,1], c='g', marker='o')
    elif sentiment_class[i] == "positive":
        c3 = pl.scatter(principalComponents[i,0], principalComponents[i,1], c='b', marker='*')
pl.legend([c1, c2, c3], ['negative', 'neutral', 'positive'])
pl.title('Trt World Twitter 3 Clusters and Sentiments')
pl.show()
```


![png](output_17_0.png)



```python
# In the last part of analysis, I modelled classification of subjectivity
# By doing this, by favorite and retweet counts and sentiment classes, I tried to predict 
# Is tweet is subjective or objective
# Firstly I encoded the categorical data and then randomly splitted to train test datasets
le=LabelEncoder()
for i in ["sentiment_class","subjectivity_class"]:
    data[i]=le.fit_transform(data[i])
y=data["subjectivity_class"]
x=data[["sentiment_class","favorites","retweets"]]
x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.3,random_state=1)
```


```python
# In classification part, I modelled three different and fundemantal methods
# First of them is Random Forest which is highly handy and strong method
# secondly, one of the main algorithm of machine learning Neighbor Classifier
# The last is Gradient Boosting Machines with decision trees
# In the first two i used scikit-learn's functions and in the latter one I used LightGBM which is introduced by Microsoft engineers

rfc=RandomForestClassifier(n_estimators=100,max_depth=7)
rfc.fit(x_train,y_train)

knn=KNeighborsClassifier()
knn.fit(x_train,y_train)

lgb=LGBMClassifier(n_estimators=1000,learning_rate=0.05,max_depth=4)
lgb.fit(x_train,y_train)
```




    LGBMClassifier(boosting_type='gbdt', class_weight=None, colsample_bytree=1.0,
            learning_rate=0.05, max_depth=4, min_child_samples=20,
            min_child_weight=0.001, min_split_gain=0.0, n_estimators=1000,
            n_jobs=-1, num_leaves=31, objective=None, random_state=None,
            reg_alpha=0.0, reg_lambda=0.0, silent=True, subsample=1.0,
            subsample_for_bin=200000, subsample_freq=0)




```python
print("Random Forest Classifier's train test dataset scores")
print("\n" + str(rfc.score(x_train,y_train)) +" and " + str(rfc.score(x_test,y_test)))
print("\n" +"K Neighborhood Classifier's train and test dataset scores")
print("\n" + str(knn.score(x_train,y_train)) + " and " + str(knn.score(x_test,y_test)))
print("\n" +"Light GBM's train and test dataset scores")
print("\n" + str(lgb.score(x_train,y_train)) +" and " +  str(lgb.score(x_test,y_test)))

```

    Random Forest Classifier's train and test dataset scores
    
    0.8190730837789661 and 0.794392523364486
    
    K Neighborhood Classifier's train and test dataset scores
    
    0.8204099821746881 and 0.7466251298026999
    
    Light GBM's train and test dataset scores
    
    0.836007130124777 and 0.7777777777777778
